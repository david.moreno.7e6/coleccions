import java.util.Scanner

fun main(){
    val sc= Scanner(System.`in`)
    println("Introdueix el numero de cartells que vols introduir:")
    var num = sc.nextInt()
    val cartells= numeroCartells(num, sc)
    println("Introdueix el numero de consultes a realitzar:")
    num = sc.nextInt()
    numeroConsultes(num,sc,cartells)
}
fun numeroCartells(num: Int, sc: Scanner): MutableMap<Int, String> {
    val cartells= mutableMapOf<Int, String>()
    for (i in 1..num){
        println("Introdueix el kilometre i el text asociat:")
        cartells.put(sc.nextInt(), sc.next())
    }
    return cartells
}

fun numeroConsultes(num: Int, sc: Scanner, cartells: MutableMap<Int,String>){
    for (i in 1..num){
        println("Numero del km del cartell:")
        val consulta= sc.nextInt()
        if (consulta in cartells){
            println("Cartell $consulta: ${cartells[consulta]}")
        }
        else {
            println("No hi ha cartell")
        }
    }
}