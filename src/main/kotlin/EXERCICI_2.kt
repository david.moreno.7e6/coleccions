import java.util.Scanner

fun main(){
    val sc= Scanner(System.`in`)
    println("Introdueix el numero d'empleats que vols introduir:")
    var num= sc.nextInt()
    val empleats= crearEmpleats(num, sc)
    consultesEmpleats( sc, empleats)
}
fun crearEmpleats(num: Int, sc: Scanner): MutableList<MutableMap<String, String>> {
    val empleats= mutableListOf<MutableMap<String, String>>()
    for (i in 1..num){
        println("Introdueix: DNI, Nom, Cognom i Adreça")
        empleats.add(mutableMapOf("DNI" to sc.next(), "Nom" to sc.next(), "Cognom" to sc.next(), "Adreça" to sc.next()))
    }
    return empleats
}
fun consultesEmpleats(sc: Scanner,empleats: MutableList<MutableMap<String,String>>){
    var consulta=""
    while (consulta != "END"){
        var consultaCorrecta= false
        println("Introdueix el DNI per fer la consulta:")
        consulta= sc.next().uppercase()
        if (consulta =="END"){
            break
        }
        for (i in 0..empleats.lastIndex){
            if (consulta in empleats[i].values){
                println(empleats[i])
                consultaCorrecta= true
            }
        }
        if(!consultaCorrecta){
            println("Empleat no enregistrat")
        }
    }
}