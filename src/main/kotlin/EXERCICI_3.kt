import java.util.Scanner

fun main(){
    val sc= Scanner(System.`in`)
    for (i in votDelegat(sc)){
        println("${i.key}: ${i.value}")
    }
}
fun votDelegat(sc: Scanner): MutableMap<String, Int>{
    var finish= false
    val votacio= mutableMapOf<String, Int>()
    while (finish==false){
        println("Quin es el te vot?")
        val vot= sc.next().uppercase()
        if (vot in votacio){
            votacio[vot] = votacio[vot]!! + 1
        }
        else if(vot.uppercase()=="END"){
            finish=true
        }
        else {
            votacio.put(vot, 1)
        }
    }
    return votacio
}