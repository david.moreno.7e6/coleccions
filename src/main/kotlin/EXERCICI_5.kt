import java.util.Scanner

fun main(){
    val sc= Scanner(System.`in`)
    val cartilla= cartillaBingo(sc)
    bingo(cartilla,sc)
}
fun cartillaBingo(sc:Scanner): MutableList<Int> {
    println("Introdueix els numeros de la tarjeta")
    val cartilla= mutableListOf<Int>()
    for (i in 1..10){
        cartilla.add(sc.nextInt())
    }
    return cartilla
}

fun bingo(cartilla: MutableList<Int>, sc: Scanner){
    while (cartilla.size !=0){
        println("Numero Bingo:")
        val numero= sc.nextInt()
        if (numero in cartilla){
            cartilla.remove(numero)
            println("Et queden ${cartilla.size} numeros")
        }
    }
    println("BINGO!")
}