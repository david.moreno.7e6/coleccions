import java.util.*

interface GymControlReader{
    fun nextId() : String
}
class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    val activeClient= mutableSetOf<String>()
    override fun nextId(): String {
        val client= scanner.next()
        if (activeClient.add(client)){
            return "$client ENTRADA"
        }
        else{
            activeClient.remove(client)
            return "$client SALIDA"
        }
    }
}
fun main(){
    val reader= GymControlManualReader()
    for ( i in 1..8){
        println(reader.nextId())
    }
}
